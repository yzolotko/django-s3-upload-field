# djaongo-s3-upload-field

![Demo](https://bitbucket.org/yzolotko/django-s3-upload-field/raw/5d7dbbd83b56feb353d0332eed2178b9eeb816f4/demo.gif)

`s3field` is django-storages compatible replacement for `django.forms.FileField` which does client-side upload to Amazon S3.

## Installation

1. Follow https://django-storages.readthedocs.io/en/latest/index.html#installation
2. `pip install git+https://yzolotko@bitbucket.org/yzolotko/django-s3-upload-field.git`
3. Add `s3field` to `INSTALLED_APPS` (required for s3field static assets).

## Usage

1.Follow https://django-storages.readthedocs.io/en/latest/backends/amazon-S3.html

2.Given there is model with `django.db.models.FileField`:


```python
class User(models.Model):
    avatar = models.FileField()
```

s3field can be used in the form like this:

```python
from s3field import S3FileField

class UserForm(forms.ModelForm):
	avatar = S3FileField(required=False, prefix='avatars/')

	class Meta:
		model = User
```
`{{form.media}}` must be included in template.

With Javascript enabled, selecting file in avatar file input, will upload it client-side to S3 and only link will be sent to Django server instead of full file contents.

With Javascript disabled, full file contents will be sent to Django server and then it will be passed to `django.forms.FileField` as-is which will in its turn upload it to S3 server-to-server.

## Alternatives
..and why yet another one.

- https://pypi.org/project/django-s3direct/ - incompatible with django-storages, see https://github.com/bradleyg/django-s3direct/issues/154 ; does not work with Javascript disabled.
- https://pypi.org/project/django-storages-s3upload/ - can't be used in existing forms or Django admin, only one input per form.
