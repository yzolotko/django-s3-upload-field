from json import dumps
from time import time

import boto3
from django.conf import settings
from django.forms import FileField
from django.forms.widgets import ClearableFileInput
from django.template.defaultfilters import filesizeformat


class S3UploaderWidget(ClearableFileInput):
    class Media:
        js = 's3field/main.js',

    def value_from_datadict(self, data, files, name):
        if name in data and data[name]:
            return True, data[name]
        return False, super(S3UploaderWidget, self).value_from_datadict(
            data, files, name)


class S3FileField(FileField):
    widget = S3UploaderWidget

    def __init__(self, *args, **kwargs):
        prefix = kwargs.pop('prefix', None)
        max_size = kwargs.pop('max_size', None)
        super(S3FileField, self).__init__(*args, **kwargs)
        self.widget.attrs[
            'data-s3-uploader'] = lambda: self.data(prefix, max_size)

    @staticmethod
    def data(prefix, max_size):
        client = boto3.client(
            's3',
            region_name=settings.AWS_S3_REGION_NAME,
            aws_access_key_id=settings.AWS_ACCESS_KEY_ID,
            aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY)
        ts = int(time())
        prefix = prefix or ''
        prefix += str(ts) + '_'
        acl = dict(acl='public-read')
        data = dict(
            prefix=prefix,
            max_size=max_size,
            **client.generate_presigned_post(
                settings.AWS_STORAGE_BUCKET_NAME,
                prefix + '${filename}',
                Conditions=[acl],
                Fields=dict(acl),
            ))
        if max_size:
            data['max_size'] = max_size
            data['max_size_hr'] = filesizeformat(max_size)
        return dumps(data)

    def clean(self, data, initial=None):
        is_url, data = data
        if is_url:
            return data
        return super(S3FileField, self).clean(data, initial)
