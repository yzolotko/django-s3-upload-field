$(function() {
    $('[data-s3-uploader]').on('change', function(e) {
        var data = JSON.parse(e.target.getAttribute('data-s3-uploader'))
        var file = this.files[0]
        if (data.max_size) {
            if (file.size > data.max_size) {
                alert('Maximum size is ' + data.max_size_hr)
                e.target.value = ''
                return
            }
        }
        var xhr = new XMLHttpRequest()
        xhr.open("POST", data.url)
        var postData = new FormData()
        for (var k in data.fields)
            postData.append(k, data.fields[k])
        postData.append('file', file)
        var id = e.target.id + '-progress'
        var progress = document.getElementById(id)
        if (progress) {
            progress.removeAttribute('max')
            progress.removeAttribute('value')
            $(progress).show()
        } else {
            progress = document.createElement('progress')
            e.target.parentElement.append(progress)
        }
        xhr.upload.onprogress = function(e) {
            if (e.lengthComputable) {
                progress.max = e.total
                progress.value = e.loaded
            }
        }
        xhr.onreadystatechange = function() {
            if (xhr.readyState === 4) {
                $(progress).hide()
                if (xhr.status === 200 || xhr.status === 204) {
                    var id = e.target.id + '-hidden'
                    var input = document.getElementById(id)
                    if (!input) {
                        input = document.createElement('input')
                        input.type = 'hidden'
                        input.id = id
                        input.name = e.target.name
                        e.target.parentElement.append(input)
                    }
                    input.value = data.prefix + file.name
                    id = e.target.id + '-link'
                    var a = document.getElementById(id)
                    if (!a) {
                        a = document.createElement('a')
                        a.id = e.target.id + '-link'
                        $(a).insertAfter(e.target)
                    }
                    a.innerText = file.name
                    a.href = data.url + '/' + data.prefix + file.name
                    e.target.value = ''
                } else {
                    alert("Could not upload file.");
                }
            }
        };
        xhr.send(postData);
    })
});
