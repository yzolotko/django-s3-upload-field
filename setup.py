from setuptools import setup

setup(
    name='s3field',
    version='0.0.1',
    description='Replacement for Django FileField '
    'for client side upload to Amazon S3.',
    url='https://bitbucket.org/yzolotko/django-s3-upload-field',
    author='Uri Zolotko',
    author_email='yurii.zolotko@gmail.com',
    license='MIT',
    packages=['s3field'],
    install_requires=[
        'boto3',
    ]
)
